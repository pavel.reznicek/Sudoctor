unit SudoctorDesktop;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, SudoctorField;

type

  { TSudoctorFieldList }
  TSudoctorFieldList = class(TSudoctorFieldListBase)
  public
    constructor Create;
  end;

  { TSudoctorDesktop }

  TSudoctorDesktop = class(TPanel)
  private
    { Private declarations }
    FList: TSudoctorFieldList;
    function AddField: Integer;
    function GetItems(Index: Integer): TSudoctorField;
    function GetItemsByPos(Position: TPoint): TSudoctorField;
    procedure FieldSelectionChanged(Sender: TObject);
    procedure FieldDblClick(Sender: TObject);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure AfterConstruction; override;
    procedure Resize; override;
    function PositionOf(Field: TSudoctorField): TPoint;
    function IndexToPos(Index: Integer): TPoint;
    function PosToIndex(Position: TPoint): Integer;
    function GetRow(Field: TSudoctorField; Inclusive: Boolean = False):
      TSudoctorFieldList;
    function GetColumn(Field: TSudoctorField; Inclusive: Boolean = False):
      TSudoctorFieldList;
    function GetSquare(Field: TSudoctorField; Inclusive: Boolean = False):
      TSudoctorFieldList;
    procedure EnableNumbersInField(Field: TSudoctorField);
    procedure EnableNumbersInList(OriginalField: TSudoctorField;
      ListToEnable: TSudoctorFieldList);
    property Items[Index: Integer]: TSudoctorField read GetItems;
    property ItemsByPos[Position: TPoint]: TSudoctorField read GetItemsByPos;
  published
    { Published declarations }
  end;

procedure Register;

function NumberFitsInList(Number: Integer; List: TSudoctorFieldList): Boolean;

implementation

procedure Register;
begin
  RegisterComponents('Sudoctor', [TSudoctorDesktop]);
end;

function NumberFitsInList(Number: Integer; List: TSudoctorFieldList): Boolean;
var
  I: Integer;
  Field: TSudoctorField;
  Match, FitsForTheField: Boolean;
begin
  Result := True;
  for I := 0 to List.Count - 1 do
  begin
    Field := List[I];
    Match := Field.ItemIndex + 1 = Number;
    FitsForTheField := not Match;
    // If the field's number doesn't match
    Result := Result and FitsForTheField;
  end;
end;

{ TSudoctorFieldList }

constructor TSudoctorFieldList.Create;
begin
  inherited Create(False);
end;

{ TSudoctorDesktop }

function TSudoctorDesktop.AddField: Integer;
var
  Field: TSudoctorField;
  Index: Integer;
begin
  Field := TSudoctorField.Create(Self);
  with Field do
  begin
    Parent := Self;
    Caption := '';
    OnSelectionChanged := @FieldSelectionChanged;
    OnDblClick := @FieldDblClick;
  end;
  Index := FList.Add(Field);
  Result := Index;
end;

procedure TSudoctorDesktop.FieldSelectionChanged(Sender: TObject);
var
  Position: TPoint;
  OriginalField: TSudoctorField;
  OriginalNumber: Integer;
  Row, Column, Square: TSudoctorFieldList;
  procedure DisableNumbersInList(List: TSudoctorFieldList);
  var
    I: Integer;
    CurrentField: TSudoctorField;
  begin
    for I := 0 to List.Count - 1 do
    begin
      CurrentField := List[I];
      Position := PositionOf(CurrentField);
      //Write('(', Position.x, '; ', Position.y, ') ');
      if OriginalNumber > 0 then
        CurrentField.Controls[OriginalNumber - 1].Enabled := False;
    end;
    //WriteLn();
  end;
begin
  Beep;
  OriginalField := Sender as TSudoctorField;
  Position := PositionOf(OriginalField);
  WriteLn('(', Position.x, '; ', Position.y, ')');
  OriginalNumber := OriginalField.ItemIndex + 1;
  //if OriginalNumber > 0 then // No, we need to reflect zeroes too
  begin
    Row := GetRow(OriginalField);
    DisableNumbersInList(Row);
    EnableNumbersInList(OriginalField, Row);
    Column := GetColumn(OriginalField);
    DisableNumbersInList(Column);
    EnableNumbersInList(OriginalField, Column);
    Square := GetSquare(OriginalField);
    DisableNumbersInList(Square);
    EnableNumbersInList(OriginalField, Square);
    Row.Free;
    Column.Free;
    Square.Free;
  end;
end;

procedure TSudoctorDesktop.FieldDblClick(Sender: TObject);
var
  Field: TSudoctorField;
begin
  Field := Sender as TSudoctorField;
  Field.Locked := not Field.Locked;
  if not Field.Locked then
  begin
    EnableNumbersInField(Field);
  end;
end;

function TSudoctorDesktop.GetItems(Index: Integer): TSudoctorField;
begin
  Result := FList.Items[Index];
end;

function TSudoctorDesktop.GetItemsByPos(Position: TPoint): TSudoctorField;
var
  Index: Integer;
begin
  Index := PosToIndex(Position);
  Result := FList.Items[Index];
end;

constructor TSudoctorDesktop.Create(AOwner: TComponent);
var
  I: Integer;
begin
  inherited Create(AOwner);
  BorderWidth := 2;
  AutoSize := True;
  FList := TSudoctorFieldList.Create;
  for I := 0 to sqr(9) - 1 do
    AddField;
end;

procedure TSudoctorDesktop.AfterConstruction;
begin
  inherited AfterConstruction;
end;

procedure TSudoctorDesktop.Resize;
var
  I: Integer;
  Field: TSudoctorField;
  FieldSize: Integer;
begin
  inherited Resize;
  for I := 0 to FList.Count - 1 do
  begin
    Field := FList[I];
    DisableAutoSizing;
    try
      FieldSize := Field.ActualSize;
      Field.Left := (I mod 9) * FieldSize + BorderWidth + 1;
      Field.Top := (I div 9) * FieldSize + BorderWidth + 1;
    finally
      EnableAutoSizing;
    end;
  end;
end;

function TSudoctorDesktop.PositionOf(Field: TSudoctorField): TPoint;
var
  Index: Integer;
begin
  Index := FList.IndexOf(Field);
  Result := IndexToPos(Index);
end;

function TSudoctorDesktop.IndexToPos(Index: Integer): TPoint;
begin
  Result.x := Index mod 9;
  Result.y := Index div 9;
end;

function TSudoctorDesktop.PosToIndex(Position: TPoint): Integer;
begin
  Result := Position.y * 9 + Position.x;
end;

function TSudoctorDesktop.GetRow(Field: TSudoctorField; Inclusive: Boolean
  ): TSudoctorFieldList;
var
  I: Integer;
  OriginalFieldPos, CurrentFieldPos: TPoint;
begin
  OriginalFieldPos := PositionOf(Field);
  Result := TSudoctorFieldList.Create;
  for I := 0 to 8 do
  begin
    if (I <> OriginalFieldPos.x) or Inclusive  then
    begin
      CurrentFieldPos := Point(I, OriginalFieldPos.y);
      Result.Add(ItemsByPos[CurrentFieldPos]);
    end;
  end;
end;

function TSudoctorDesktop.GetColumn(Field: TSudoctorField; Inclusive: Boolean
  ): TSudoctorFieldList;
var
  I: Integer;
  OriginalFieldPos, CurrentFieldPos: TPoint;
begin
  OriginalFieldPos := PositionOf(Field);
  Result := TSudoctorFieldList.Create;
  for I := 0 to 8 do
  begin
    if (I <> OriginalFieldPos.y) or Inclusive  then
    begin
      CurrentFieldPos := Point(OriginalFieldPos.x, I);
      Result.Add(ItemsByPos[CurrentFieldPos]);
    end;
  end;
end;

function TSudoctorDesktop.GetSquare(Field: TSudoctorField; Inclusive: Boolean
  ): TSudoctorFieldList;
var
  X, Y: Integer;
  OriginalFieldPos, SquareBasePos, CurrentFieldPos: TPoint;
begin
  OriginalFieldPos := PositionOf(Field);
  SquareBasePos := Point(
    OriginalFieldPos.x - OriginalFieldPos.x mod 3,
    OriginalFieldPos.y - OriginalFieldPos.y mod 3
  );
  Result := TSudoctorFieldList.Create;
  for Y := SquareBasePos.y to SquareBasePos.y + 2 do
  begin
    for X := SquareBasePos.x to SquareBasePos.x + 2 do
    begin
      if ((X <> OriginalFieldPos.x) or (Y <> OriginalFieldPos.y)) or Inclusive
      then
      begin
        CurrentFieldPos := Point(X, Y);
        Result.Add(ItemsByPos[CurrentFieldPos]);
      end;
    end;
  end;
end;

procedure TSudoctorDesktop.EnableNumbersInField(Field: TSudoctorField);
var
  K: Integer;
  EnablementRow, EnablementColumn, EnablementSquare: TSudoctorFieldList;
  FitsInRow, FitsInColumn, FitsInSquare, Fits: Boolean;
begin
  EnablementRow := GetRow(Field);
  EnablementColumn := GetColumn(Field);
  EnablementSquare := GetSquare(Field);
  for K := 1 to 9 do
  begin
    FitsInRow := NumberFitsInList(K, EnablementRow);
    FitsInColumn := NumberFitsInList(K, EnablementColumn);
    FitsInSquare := NumberFitsInList(K, EnablementSquare);
    Fits := FitsInRow and FitsInColumn and FitsInSquare;
    // Enable the number
    if Fits then
      Field.Controls[K - 1].Enabled := True;
  end;
  EnablementRow.Free;
  EnablementColumn.Free;
  EnablementSquare.Free;
end;

procedure TSudoctorDesktop.EnableNumbersInList(OriginalField: TSudoctorField;
  ListToEnable: TSudoctorFieldList);
var
  J: Integer;
  CurrentField: TSudoctorField;
begin
  for J := 0 to ListToEnable.Count -1 do
  begin
    CurrentField := ListToEnable[J];
    if (not (CurrentField = OriginalField)) {skip the changed field}
    and (not CurrentField.Locked) {skip the locked fields} then
    begin
      EnableNumbersInField(CurrentField);
    end;
  end;
end;


end.
