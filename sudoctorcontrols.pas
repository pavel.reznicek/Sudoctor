{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit SudoctorControls;

interface

uses
  SudoctorField, SudoctorDesktop, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('SudoctorField', @SudoctorField.Register);
  RegisterUnit('SudoctorDesktop', @SudoctorDesktop.Register);
end;

initialization
  RegisterPackage('SudoctorControls', @Register);
end.
