unit umain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  SudoctorDesktop, uplaysound;

type

  { TFrmMain }

  TFrmMain = class(TForm)
    playsound: Tplaysound;
    SudoctorDesktop1: TSudoctorDesktop;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

procedure DoBeep;

var
  FrmMain: TFrmMain;

implementation

{$R *.lfm}

{ TFrmMain }

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  OnBeep := @DoBeep;
end;

procedure TFrmMain.FormShow(Sender: TObject);
begin
  FrmMain.ClientWidth := SudoctorDesktop1.Width;
  FrmMain.ClientHeight:= SudoctorDesktop1.Height;
end;

procedure DoBeep;
begin
  FrmMain.playsound.SoundFile := ExtractFilePath(Application.ExeName) +
    //'pyp.wav';
    'click.wav';
  FrmMain.playsound.Execute;
end;

end.

