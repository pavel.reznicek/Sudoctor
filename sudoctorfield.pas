unit SudoctorField;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, fgl;

type

  { TSudoctorField }

  TSudoctorField = class(TRadioGroup)
  private
    { Private declarations }
    tmrDblClick: TTimer;
    FLocked: Boolean;
    FOnDblClick: TNotifyEvent;
    FOnLockedChange: TNotifyEvent;
    procedure RadioButtonMouseDown(Sender: TObject; Button: TMouseButton; Shift:
        TShiftState; X, Y: Integer);
    procedure SetLocked(AValue: Boolean);
    procedure tmrDblClickTick(Sender: TObject);
  protected
    { Protected declarations }
    procedure CalculatePreferredSize(var PreferredWidth,
        PreferredHeight: integer; WithThemeSpace: Boolean); override;
    procedure DoSetBounds(ALeft, ATop, AWidth, AHeight: integer); override;
    procedure DblClick; override;
    procedure Click; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    function GetActualSize: Integer;
  published
    { Published declarations }
    property ActualSize: Integer read GetActualSize;
    property OnDblClick: TNotifyEvent read FOnDblClick write FOnDblClick;
    property Locked: Boolean read FLocked write SetLocked;
    property OnLockedChange: TNotifyEvent read FOnLockedChange
      write FOnLockedChange;
  end;

  TSudoctorFieldListBase = specialize TFPGObjectList<TSudoctorField>;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Sudoctor',[TSudoctorField]);
end;

{ TSudoctorField }

procedure TSudoctorField.CalculatePreferredSize(var PreferredWidth,
  PreferredHeight: integer; WithThemeSpace: Boolean);
begin
  inherited CalculatePreferredSize(PreferredWidth, PreferredHeight,
    WithThemeSpace);
  PreferredHeight:=Width;
end;

procedure TSudoctorField.DoSetBounds(ALeft, ATop, AWidth, AHeight: integer);
begin
  inherited DoSetBounds(ALeft, ATop, AWidth, AHeight);
  InvalidatePreferredSize;
end;

procedure TSudoctorField.DblClick;
begin
  inherited DblClick;
  //ShowMessage('Double Click');
end;

procedure TSudoctorField.Click;
begin
  inherited Click;
  if not FLocked then
  begin
    ItemIndex := -1;
    if Assigned(OnSelectionChanged) then
      OnSelectionChanged(Self);
  end;
end;

procedure TSudoctorField.RadioButtonMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then
  begin
    if tmrDblClick.Enabled then
    begin
      if Assigned(FOnDblClick) then
        FOnDblClick(Self);
      //ShowMessage('Double Click');
      //Beep;
      tmrDblClick.Enabled := False;
    end
    else
    begin
      tmrDblClick.Enabled := True;
    end;
  end;
end;

procedure TSudoctorField.SetLocked(AValue: Boolean);
var
  I: Integer;
  Radio: TRadioButton;
begin
  if FLocked=AValue then Exit;
  FLocked:=AValue;

  // Set the color
  if FLocked then
    Color := clSkyBlue
  else
    color := clDefault;

  { If locked, disable all the radio buttons.
    If unlocked, enable all the buttons. }
  for I := 0 to ControlCount - 1 do
  begin
    Radio := Controls[I] as TRadioButton;
    if not Radio.Checked then
      Radio.Enabled := not FLocked;
  end;

  // Fire the event
  if Assigned(FOnLockedChange) then
    FOnLockedChange(Self);
end;

procedure TSudoctorField.tmrDblClickTick(Sender: TObject);
begin
  tmrDblClick.Enabled := False;
end;

constructor TSudoctorField.Create(AOwner: TComponent);
var
  I: Integer;
begin
  inherited Create(AOwner);
  Columns := 3;
  for I := 1 to 9 do
  begin
    Items.Append('&' + IntToStr(I));
    (Controls[I - 1] as TRadioButton).OnMouseDown := @RadioButtonMouseDown;
  end;
  tmrDblClick := TTimer.Create(Self);
  tmrDblClick.Interval := 100;
  tmrDblClick.OnTimer := @tmrDblClickTick;
  Caption := '';
  AutoSize := True;
end;

function TSudoctorField.GetActualSize: Integer;
var
  tmpHeight, tmpWidth: Integer;
begin
  tmpHeight := 0;
  tmpWidth := 0;
  //InvalidatePreferredSize;
  HandleNeeded;
  CalculatePreferredSize(tmpHeight, tmpWidth, False);
  Result := tmpWidth;
end;

end.
